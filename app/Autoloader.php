<?php

// On définit le raccourcis du chemin qui permettra d'accéder à ce fichier Autoloader.php
namespace app;
//On créé une class qui permettra de retourner/afficher la vue demandée
class Autoloader {
    /**
     * Cette fonction est qualifiée de static car elle sera toujours constante. "Static" l'a rend accessible de l'extérieur.
     */
    static function register() {
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    /**
     * Ce paramètre $class correspond à celui passé en paramètre de la fonction register()
     * @param $class string
     * La fonction retourne à travers le require le lien du fichier dont la class est passée en paramètre $class
     */
    static function autoload($class) {
        if (strpos($class, __NAMESPACE__ . '\\') === 0) {
            $class = str_replace(__NAMESPACE__ . '\\', '', $class);
            $class = str_replace('\\', '/', $class);
            require __DIR__ . '/' . $class . '.php';
        }
    }
}
