<?php

namespace app\controllers;



class Post {

    public function showPost($id){

        $getAllPosts = new \app\models\Post();
        return $getAllPosts->getPost($id);
    }

    public function getAllPosts($rub){
        $getAllPosts = new \app\models\Post();
        return $getAllPosts->getAllPosts($rub);
    }

    public function countAllPosts(){
        $countAllPosts = new \app\models\Post();
        return $countAllPosts->countAllPosts();
    }

    public function insertPost ($titre,$image,$description,$contenu,$rub){

        $backModel = new \app\models\Post();
        $backModel->insertPost($titre,$image,$description,$contenu,$rub);
    }

    public function updatePost($titre,$image,$description,$contenu,$id){

        $backModel = new \app\models\Post();
        $backModel->updatePost($titre,$image,$description,$contenu,$id);

    }

    public function deletePost($id){

        $backModel = new \app\models\Post();
        $backModel->deletePost($id);
    }

    public function bol ($onoff,$id){

        $backModel = new \app\models\Post();
        $backModel->bol($onoff,$id);
    }

}