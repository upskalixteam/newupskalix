<?php

namespace app\controllers;

class myAccount {

    public function updatePwd($id, $pwd){

        $newPwd = password_hash($pwd,  PASSWORD_BCRYPT);

        $pwdChange = new \app\models\myAccount();
        $pwdChange->updatePwd($id, $newPwd);

    }

}