<?php

namespace app\controllers;

class Rubriques {

    public function getAllRubriques(){
        $getAllRubriques = new \app\models\Rubriques();
        return $getAllRubriques->getAllRubriques();
    }
    public function insertRubriques($r_nom){
        $backModel = new \app\models\Rubriques();
        $backModel->insertRubriques($r_nom);
    }
    public function showRubriques($id){
        $getRubriques = new \app\models\Rubriques();
        return $getRubriques->getRubriques($id);
    }
    public function updateRubriques($r_nom,$id){
        $backModel = new \app\models\Rubriques();
        $backModel->updateRubriques($r_nom,$id);
    }
    public function deleteRubriques($id){
        $backModel = new \app\models\Rubriques();
        $backModel->deleteRubriques($id);
    }

}