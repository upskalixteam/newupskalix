<?php

namespace app\controllers;

class subscribers {
    public function addSubscriber($mail){
        $resQuery = new \app\models\subscribers();
        $resQuery->addSubscriber($mail);
    }

    public function verifySubscriber($mail){
        $resQuery = new \app\models\subscribers();
        return $resQuery->verifySubscriber($mail);
    }

    public function getSubscribers(){
        $resQuery = new \app\models\subscribers();
        return $resQuery->getSubscribers();
    }

    public function deleteSubscriber($id){
        $resQuery = new \app\models\subscribers();
        $resQuery->deleteSubscriber($id);
    }
}
