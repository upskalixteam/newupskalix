<?php
namespace app\controllers;


class adlogo {

    const FORM_IMAGE_OK = 0;//constante statique par default
    const FORM_IMAGE_EXIST = 1;
    const FORM_IMAGE_KO = 2;

    public function addLogo($logoUpl) {

        $ret = adlogo::FORM_IMAGE_KO;//image ko par default


        if (file_exists(IMAGES_PATH . '/rubriquePartners/' . $logoUpl['name'])) {//chemin de l'image

            $ret = adlogo::FORM_IMAGE_EXIST;

        } else {

            if (!is_dir(IMAGES_PATH . '/rubriquePartners')) {//chemin du dossier

                mkdir(IMAGES_PATH . '/rubriquePartners');// si il n'existe pas creer le dossier

            }

            move_uploaded_file($logoUpl['tmp_name'], IMAGES_PATH . '/rubriquePartners/' . $logoUpl['name']);//chemin final de l'image

            $ret = adlogo::FORM_IMAGE_OK;
        }

        return $ret;

    }

}