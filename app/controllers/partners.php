<?php

namespace app\controllers;

class Partners {

    public function showPartners($id){
        $getAllPartners = new \app\models\Partners();
        return $getAllPartners->getPartners($id);
    }

    public function getAllPartners(){
        $getAllPartners = new \app\models\Partners();
        return $getAllPartners->getAllPartners();
    }

    public function insertPartners ($nom,$logo,$image, $description){
        $backModel = new \app\models\Partners();
        $backModel->insertPartners($nom,$logo,$image, $description);
    }

    public function updatePartners($nom,$logo,$image, $description,$id){
        $backModel = new \app\models\Partners();
        $backModel->updatePartners($nom,$logo,$image, $description,$id);
    }

    public function deletePartners($id){
        $backModel = new \app\models\Partners();
        $backModel->deletePartners($id);
    }

}
