<?php

namespace app\controllers;

class user {

    public function loginUser($login, $pwd){

        $modelUser = new \app\models\user();

        $currentUser = $modelUser->getUser($login);

            if($currentUser){

                $isVerify = password_verify($pwd, $currentUser->mdp);

                    if($isVerify){
                        $_SESSION['user'] = $currentUser;
                    }

                return $isVerify;
            }

        return false;
    }

}
