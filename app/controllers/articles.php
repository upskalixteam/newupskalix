<?php

namespace app\controllers;



class Articles {

    public function showArticle($idArticle){
        $getAllArticles = new \app\models\Articles();
        return $getAllArticles->getArticle($idArticle);
    }

    public function labelArticle(){
        $labelArticle = new \app\models\Articles();
        return $labelArticle->labelArticle();
    }
    public function getAllArticles(){
        $getAllArticles = new \app\models\Articles();
        return $getAllArticles->getAllArticles();
    }
    public function getLastArticle(){
        $getLastArticle = new \app\models\Articles();
        return $getLastArticle->getLastArticle();
    }
    public function getSecondLastArticle(){
        $getThirdLastArticles = new \app\models\Articles();
        return $getThirdLastArticles->getSecondLastArticle();
    }
    public function getThirdLastArticles(){
        $getThirdLastArticles = new \app\models\Articles();
        return $getThirdLastArticles->getThirdLastArticles();
    }
    public function getThirdAndFourthLastArticles(){
        $getThirdLastArticles = new \app\models\Articles();
        return $getThirdLastArticles->getThirdAndFourthLastArticles();
    }
    public function getFifthLastArticle(){
        $getThirdLastArticles = new \app\models\Articles();
        return $getThirdLastArticles->getFifthLastArticle();
    }
    public function getAllLastArticlesAfterFifth(){
        $getThirdLastArticles = new \app\models\Articles();
        return $getThirdLastArticles->getAllLastArticlesAfterFifth();
    }


}



