<?php



namespace app\controllers;


class adimg {

    const FORM_IMAGE_OK = 0;//constante statique par default
    const FORM_IMAGE_EXIST = 1;
    const FORM_IMAGE_KO = 2;

    public function addImage($imageUpl) {

        $ret = adimg::FORM_IMAGE_KO;//image ko par default


        if (file_exists(IMAGES_PATH . '/blog/' . $imageUpl['name'])) {//chemin de l'image
            $ret = adimg::FORM_IMAGE_EXIST;
        } else {
            if (!is_dir(IMAGES_PATH . '/blog')) {//chemin du dossier
                mkdir(IMAGES_PATH . '/blog');// si il n'existe pas creer le dossier
            }
            move_uploaded_file($imageUpl['tmp_name'], IMAGES_PATH . '/blog/' . $imageUpl['name']);//chemin final de l'image

            $ret = adimg::FORM_IMAGE_OK;
        }

        return $ret;
    }

}