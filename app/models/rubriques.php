<?php

namespace app\models;

class Rubriques{

    private $sqlQuery;

    public function dbConnect(){

        if($this->sqlQuery === NULL){
            $this->sqlQuery = \app\models\Connect::getDbConnect();
        }
        return $this->sqlQuery;
    }

    public function getAllRubriques(){

        $sql = 'SELECT * FROM rubriques';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    public function insertRubriques($r_nom){

        $sql= 'INSERT INTO rubriques (r_nom,date) VALUES (:r_nom, NOW())';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':r_nom',$r_nom);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }
    public function getRubriques($id){
        $sql = 'SELECT * FROM rubriques WHERE r_id=:id';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':id',$id);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }
    public function updateRubriques ($r_nom,$id){

        $sql= 'UPDATE rubriques SET r_nom=:r_nom,date= NOW() WHERE r_id=:id' ;
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':r_nom',$r_nom);
        $query->bindParam(':id',$id);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }
    public function deleteRubriques ($id){

        $sql= 'DELETE FROM rubriques WHERE r_id = :id';
        $query=$this->dbConnect()->prepare($sql);
        $query->bindParam(':id',$id);
        $query->execute();
    }

}


