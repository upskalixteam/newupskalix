<?php

namespace app\models;

class Articles{

    private $sqlQuery;

    public function dbConnect(){

        if($this->sqlQuery === NULL){
            $this->sqlQuery = \app\models\Connect::getDbConnect();
        }
        return $this->sqlQuery;
    }

    /*
    *  Return all Articles
    *
    * */
    public function getAllArticles(){
        $sql = 'SELECT * FROM articles AS a LEFT JOIN rubriques AS r ON a.fk_r_id = r.r_id WHERE onoff = 1';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    /*
    *
    *  Return the second last article
    *
    * */
    public function getSecondLastArticle(){
        $sql = 'SELECT * FROM articles AS a LEFT JOIN rubriques AS r ON a.fk_r_id = r.r_id WHERE onoff = 1 ORDER BY id DESC LIMIT 1, 1 ';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS)[0];
    }

    /*
    *
    *  Return the third last articles
    *
    * */
    public function getThirdLastArticles(){
        $sql = 'SELECT * FROM articles AS a LEFT JOIN rubriques AS r ON a.fk_r_id = r.r_id WHERE onoff = 1 ORDER BY id DESC LIMIT 1, 3';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    /*
    *
    *  Return the third and the fourth last articles
    *
    * */
    public function getThirdAndFourthLastArticles(){
        $sql = 'SELECT * FROM articles AS a LEFT JOIN rubriques AS r ON a.fk_r_id = r.r_id WHERE onoff = 1 ORDER BY id DESC LIMIT 2, 2';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    /*
    *
    *  Return the fifth last article
    *
    * */
    public function getFifthLastArticle(){
        $sql = 'SELECT * FROM articles AS a LEFT JOIN rubriques AS r ON a.fk_r_id = r.r_id WHERE onoff = 1 ORDER BY id DESC LIMIT 4, 1';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS)[0];
    }

    /*
    *
    *  Return all Articles in BDD after the fifth descending to 0
    *
    * */
    public function getAllLastArticlesAfterFifth(){
        $sql = 'SELECT * FROM articles AS a LEFT JOIN rubriques AS r ON a.fk_r_id = r.r_id WHERE onoff = 1 ORDER BY id DESC LIMIT 5, 6';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    /*
    *
    *  Return the last Article
    *
    * */
    public function getLastArticle(){
        $sql = 'SELECT * FROM articles AS a LEFT JOIN rubriques AS r ON a.fk_r_id = r.r_id WHERE onoff = 1 ORDER BY id DESC LIMIT 1';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS)[0];
    }

    /*
     *
     *  Return Article according to parametre
     *
     * */

    public function getArticle($idArticle){
        $sql = 'SELECT * FROM articles WHERE onoff = 1 AND id=:id';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':id',$idArticle);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC)[0];
    }

    /*
     *
     * Return Label Article to define rubrique type and add label style
     *
     * */

    public function labelArticle(){
        $sql = 'SELECT r_nom FROM articles LEFT JOIN rubriques ON fk_r_id = r_id';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);


    }



}