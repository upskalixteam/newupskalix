<?php

namespace app\models;

class Post{

    private $sqlQuery;

    public function dbConnect(){

        if($this->sqlQuery === NULL){
            $this->sqlQuery = \app\models\Connect::getDbConnect();
        }
        return $this->sqlQuery;
    }

    public function getAllPosts($rub){

        $sql = 'SELECT * FROM articles WHERE fk_r_id=:fk_r_id';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':fk_r_id',$rub);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    public function countAllPosts(){
        $sql = 'SELECT * FROM articles';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    public function getPost($id){

        $sql = 'SELECT * FROM articles WHERE id=:id';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':id',$id);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }
    public function insertPost ($titre,$path, $description, $contenu,$rub){

        $sql= 'INSERT INTO articles (titre,image, description,contenu, date,fk_r_id) VALUES (:titre,:image,:description,:contenu, NOW(),:rub)';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':titre',$titre);
        $imagePath = explode(".", $path); // Array of the file name delimited by the dot
        $query->bindParam(':image', $imagePath[0]); // Take file name without extension
        /*$query->bindParam(':image', $path);*/
        $query->bindParam(':description',$description);
        $query->bindParam(':contenu',$contenu);
        $query->bindParam(':rub',$rub);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }


    public function updatePost ($titre,$image,$description,$contenu,$id){

        $sql= 'UPDATE articles SET titre=:titre,image=:image,description=:description,contenu=:contenu,date= NOW() WHERE id=:id' ;
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':titre',$titre);
        $query->bindParam(':image', $image);
        $query->bindParam(':description',$description);
        $query->bindParam(':contenu',$contenu);
        $query->bindParam(':id',$id);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    public function deletePost ($id){

        $sql= 'DELETE FROM articles WHERE id = :id';
        $query=$this->dbConnect()->prepare($sql);
        $query->bindParam(':id',$id);
        $query->execute();
    }

    public function bol($onoff, $id){
        $sql= 'UPDATE articles SET onoff=:onoff WHERE id=:id';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':onoff',$onoff);
        $query->bindParam(':id',$id);
        $query->execute();
        $query->fetchAll(\PDO::FETCH_CLASS);
    }


}