<?php

namespace app\models;

class subscribers {

    private $sqlQuery;

    public function dbConnect(){
        if($this->sqlQuery === NULL){
            $this->sqlQuery = \app\models\Connect::getDbConnect();
        }
        return $this->sqlQuery;
    }

    public function addSubscriber($mail){
        $sql= 'INSERT INTO abonnes(mail) VALUES (:mail)';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':mail',$mail);
        $query->execute();
        $query->fetchAll(\PDO::FETCH_CLASS);
    }

    public function verifySubscriber($mail){
        $sql = 'SELECT mail FROM abonnes WHERE mail = :mail';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':mail',$mail);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    public function getSubscribers(){
        $sql = 'SELECT * FROM abonnes';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }

    public function deleteSubscriber($id){
        $sql= 'DELETE FROM abonnes WHERE id = :id';
        $query=$this->dbConnect()->prepare($sql);
        $query->bindParam(':id',$id);
        $query->execute();
    }
}