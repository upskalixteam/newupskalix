<?php

namespace app\models;

class myAccount{

    private $sqlQuery;

    public function dbConnect(){
        if($this->sqlQuery === NULL){
            $this->sqlQuery = \app\models\Connect::getDbConnect();
        }
        return $this->sqlQuery;
    }

    public function updatePwd($id ,$pwd){


        $sql = 'UPDATE administrateurs SET mdp = :pwd WHERE id = :id';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':id', $id);
        $query->bindParam(':pwd', $pwd);
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_CLASS)[0];

    }

}