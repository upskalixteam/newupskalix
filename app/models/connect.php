<?php
//création d'une classe comprenant une  fonction statique qui vas permettre de se connecter à la base de donées.
namespace app\models;

class Connect {
    public static function getDbConnect() {
        $options = array(
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        );
        return new \PDO('mysql:host=localhost;dbname=upskalix;charset=UTF8', 'root', '',$options);
    }
}