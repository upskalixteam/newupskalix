<?php

namespace app\models;

class user{

    private $sqlQuery;

    public function dbConnect(){
        if($this->sqlQuery === NULL){
            $this->sqlQuery = \app\models\Connect::getDbConnect();
        }
        return $this->sqlQuery;
    }

    public function getUser($login){

        $sql = 'SELECT * FROM administrateurs WHERE login = :login';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':login', $login);
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_CLASS)[0];

    }

}

