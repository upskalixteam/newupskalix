<?php

namespace app\models;

class Partners{

    private $sqlQuery;

    public function dbConnect(){
        if($this->sqlQuery === NULL){
            $this->sqlQuery = \app\models\Connect::getDbConnect();
        }
        return $this->sqlQuery;
    }
    public function getAllPartners(){
        $sql = 'SELECT * FROM partenaires';
        $query = $this->dbConnect()->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }
    public function getPartners($id){
        $sql = 'SELECT * FROM partenaires WHERE id=:id';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':id',$id);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }
    public function insertPartners ($nom,$logo, $description){
        $sql= 'INSERT INTO partenaires (nom,logo, description) VALUES (:nom,:logo,:description)';
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':nom',$nom);
        $query->bindParam(':logo', $logo);
        $query->bindParam(':description',$description);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }
    public function updatePartners ($nom,$logo,$description,$id){
        $sql= 'UPDATE partenaires SET nom=:nom,logo=:logo,description=:description WHERE id=:id' ;
        $query = $this->dbConnect()->prepare($sql);
        $query->bindParam(':nom',$nom);
        $query->bindParam(':logo', $logo);
        $query->bindParam(':description',$description);
        $query->bindParam(':id',$id);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS);
    }
    public function deletePartners ($id){
        $sql= 'DELETE FROM partenaires WHERE id = :id';
        $query=$this->dbConnect()->prepare($sql);
        $query->bindParam(':id',$id);
        $query->execute();
    }
}