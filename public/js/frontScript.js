/* ************************ */
/*                          */
/*   NAVIGATION JAVASCRIPT   */
/*                          */
/* ************************ */
/*          SUB NAV         */
/* ************************ */

/*$('#site-sub-navbar-list').hide();

$('#a-propos-link').on('mouseover', function(){
    $('#site-sub-navbar-list').delay(200).slideDown().css('background-color','#fff');
});

$('#a-propos-link').on('mouseleave', function(){
    $('#site-sub-navbar-list').hide();
});*/

/* ************************ */
/*                          */
/*   HOME PAGE JAVASCRIPT   */
/*                          */
/* ************************ */
/*      ACTIVITIES TABS     */
/* ************************ */
$('.home-section-activitiesDescription-item').hide();
$('.home-section-activitiesDescription-item:first').addClass('active');
$('#hsa-list li:first').addClass('active');
$('#hsaD-list li:first').show();
$('#hsa-list li:first').find('a').addClass('active');

$('#hsa-list li[role=tab]').on('click',function(){

    $('.home-section-activitiesDescription-item').hide();

    var currentActivitiesTab =  $(this);
    var currentActivitiesTabBtn = $(this).find('a');
    var activitiesSiblingsTabs=  $(currentActivitiesTab).siblings();
    var activitiesSiblingsTabsBtn=  $(currentActivitiesTab).siblings().find('a');

    $(activitiesSiblingsTabs).removeClass('active');
    $(activitiesSiblingsTabsBtn).removeClass('active');
    $(currentActivitiesTab).addClass('active');
    $(currentActivitiesTabBtn).addClass('active');


//     SHOW THE PARTNER TABCONTENT WITH THE ID CORRESPONDING TO THE VALUE OF THE HREF ATTRIBUTE OF THE CLICK
    var activeActivityTab = $(this).attr('aria-controls');
    //alert(activeActivityTab); // test
    $('#' + activeActivityTab).fadeIn(1000);

    return false;
});

/* ************************ */
/*      PROJECTS SLIDER     */
/* ************************ */

/*var slider = setInterval(function(){
    var sliderLength = $('#hsp-slider li').length -1;
    /!*    var next = $('#hsp-slider li.active').next();
        var last = $('#hsp-slider li').last();*!/
    var current = 0;
    if (current === 0){
        for (current = 0; current <= sliderLength; current++){

            $('#hsp-slider').children(current).addClass('active');
            // console.log(current);

        }
    }else{
        current = 0;
    }
}, 2000);*/

var projectSlider = $(function(){
    $('.home-section-projectsDescription-item').hide();
    $('.home-section-projectsDescription-item:first').show();
    $('.home-section-projects-controlBtnItem:first').addClass('active');
    setInterval(function(){
        $('.home-section-projectsDescription-item').hide(1000);
        $('.home-section-projects-controlBtnItem').removeClass('active');

        var slideWidth = $('.home-section-projects-item').width();
        $("#hsp-slider").animate({
            marginLeft:-slideWidth
        },2000,function(){
            $(this).css({
                marginLeft:0
            }).find("li:last").after($(this).find("li:first"));
            $(this).find('li:first').addClass('active');
            var projectSlideTabpanel = $('.home-section-projects-item.active').attr('aria-controls');
            $('#' + projectSlideTabpanel).fadeIn(1000);
            $('#' + projectSlideTabpanel + '-controlBtn').addClass('active');
        })
    }, 5000);
    var slidesControlsBtns = function(){
        $('home-section-projects-controlBtnItem').on('click', function () {
            var projectSlideTabBtnControl = $(this).attr('aria-controls');
            $('#' + projectSlideTabBtnControl).fadeIn(1000);
            alert(projectSlideTabBtnControl);

            $('.home-section-projects-item').attr('aria-controls = ' + projectSlideTabBtnControl).show(1000).addClass('active');
        })
    }
});



/* ************************ */
/*                          */
/* PARTNERS PAGE JAVASCRIPT */
/*                          */
/* ************************ */
/*       PARTNERS GRID      */
/* ************************ */
let scrollDownSpeed = 1500;

$('.partnersGrid-tabContent').hide();
$('.partnersGrid-item').on('click',function(){

//     HIDE ALL TABS CONTENT
    $('.partnersGrid-tabContent').hide();

//     SCROLL ANIMATION TO THE TOP OF THE DESCRIPTION BOX
    var boxDescribePartner = $('#boxDescribePartner');
    $('html, body').animate( { scrollTop: $(boxDescribePartner).offset().top }, scrollDownSpeed );

//     SHOW THE PARTNER TABCONTENT WITH THE ID CORRESPONDING TO THE VALUE OF THE HREF ATTRIBUTE OF THE CLICK
    var activeTab = $(this).find('a').attr('href');
    // alert(activeTab); // test
    $(activeTab).fadeIn(1000);

    return false;
});

/* ********************************************
/*                                              */
/*            ABOUT PAGE JAVASCRIPT             */
/*                                              */
/* ******************************************** */
/* DISPLAY ANIMATION ON NEWSLETTER SUBSCRIPTION */
/* ******************************************** */

$('#gaw-tileNewsletterSubscription-ctaBtn').on('click', function(){
    $(this).css({
        'transform':'translateX(300%)'
    });
    $('#gaw-mailSubscription-wrapper').css({
        'opacity': '1'
    });
    $('#gaw-tileNewsletterSubscription-submitBtn').css({
        'transform': 'translateX(0)'
    });
    return false;
});




