$("#admin-nav").hover(function(){
    if( $(".main-nav-legend").hasClass("open-nav") ){
        $(".main-nav-legend").removeClass("open-nav");
    }else{
        $(".main-nav-legend").addClass("open-nav");
    }


});

$(".admin-nav img").hover(function(){
    $("#admin-subnav a").css("border-top", "1px solid white");
});

// Masque la section du back office de gestion des langues
// Section n'est pas développée pour le moment
$('#hideLanguagesSection').hide();
