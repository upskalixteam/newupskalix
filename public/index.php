<?php
session_start();
// On inclut le fichier Autoloader qui se trouve dans app/views
require '../app/Autoloader.php';

// On fait appel à la méthode static register qui appartient à la classe Autoloader
app\Autoloader::register();

//On détermine quelle sera la page affichée en fonction du paramètre passé en dans l'URL en GET

//Chemin insertion image
define('PUBLIC_PATH', __DIR__);
define('IMAGES_PATH',PUBLIC_PATH.'/../assets/img/imgFront');
/*define('IMAGES_PATH',PUBLIC_PATH.'/img');*/



ob_start();

if (isset($_GET['p'])) {
    $page = $_GET['p'];

} elseif (isset($_POST['p'])) {
    $page = $_POST['p'];

} else {
    $page = 'home';

}

$layoutPath = NULL;

if (isset($page)) {
    if ($page === 'home') {
        require '../app/views/home.phtml';
        $layoutPath = "defaultHome.phtml";
        $pageTitle =  'Home | Upskalix';

    }elseif ($page === 'partenaires') {
        require '../app/views/partners.phtml';
        $layoutPath = "default.phtml";
        $pageTitle =  'Partenaires | Upskalix';

    }elseif ($page === 'aPropos') {
        require '../app/views/aPropos.phtml';
        $layoutPath = "default.phtml";
        $pageTitle =  'A propos | Upskalix';

    }elseif ($page === 'contact') {
        require '../app/views/contact.phtml';
        $layoutPath = "default.phtml";
        $pageTitle =  'Contact | Upskalix';

    }elseif ($page === 'rubrique') {
        require '../app/views/rubrique.phtml';
        $layoutPath = "default.phtml";
        $pageTitle =  'Rubrique | Upskalix';

    }elseif ($page === 'article') {
        require '../app/views/articleView.phtml';
        $layoutPath = "default.phtml";
        $pageTitle =  'Article | Upskalix';

    }elseif ($page === 'login') {
        require '../app/views/formLogin.phtml';
        $layoutPath = "loginLayout.phtml";
        $pageTitle =  'Se connecter | Upskalix';


        // On donne l'accès à la partie Admin si $_SESSION est ok
    }elseif(isset($_SESSION['user'])){

        if ($page === 'adminHome') {
            require '../app/views/adminHome.phtml';
            $layoutPath = "adminLayout.phtml";
            $adminTitle =  'Bienvenue dans votre espace de gestion';

        }elseif ($page === 'adminRubriques') {
            require '../app/views/adminRubriques.phtml';
            $layoutPath = "adminLayout.phtml";
            $adminTitle =  'Vos rubriques';

        }elseif ($page === 'adminCreateRubriques') {
            require '../app/views/adminCreateRubriques.phtml';
            $layoutPath = "adminLayout.phtml";
            $adminTitle =  'Gérer vos rubriques';

        }elseif ($page === 'adminPosts') {
            require '../app/views/adminPosts.phtml';
            $layoutPath = "adminLayout.phtml";
            $adminTitle =  'Vos articles';


        }elseif ($page === 'adminSubscribers') {
            require '../app/views/adminSubscribers.phtml';
            $layoutPath = "adminLayout.phtml";
            $adminTitle =  'Gérer vos abonnés';

        } elseif ($page === 'adminCreatePost') {
            require '../app/views/adminCreatePost.phtml';
            $layoutPath = "adminLayout.phtml";
            $adminTitle =  'Gérer vos articles';


        }elseif ($page === 'adminAbonnes') {
            require '../app/views/adminAbonnes.phtml';
            $layoutPath = "adminLayout.phtml";


        }elseif ($page === 'adminPartners') {
            require '../app/views/adminPartners.phtml';
            $layoutPath = "adminLayout.phtml";
            $adminTitle =  'Vos partenaires';


        }elseif ($page === 'adminCreatePartners') {
            require '../app/views/adminCreatePartners.phtml';
            $layoutPath = "adminLayout.phtml";
            $adminTitle =  'Gérer vos partenaires';



        }elseif ($page === 'adminLangues') {
            require '../app/views/adminLangues.phtml';
            $layoutPath = "adminLayout.phtml";

        }elseif ($page === 'adminUser') {
            require '../app/views/adminUser.phtml';
            $layoutPath = "adminLayout.phtml";
            $adminTitle =  'Gérer votre compte';           

        }elseif ($page === 'logout') {
            require '../app/views/logout.phtml';
            $layoutPath = "adminLayout.phtml";
        }
    }



} else {
    require '../app/views/home.phtml';
}

$content = ob_get_clean();

require '../app/views/layouts/'.$layoutPath;
